---
title: 'Managing an Online Class'
media_order: learning-3245793_1280.jpg
taxonomy:
    category: docs
---

# Managing an Online Class

This chapter dives into the many tasks required to set up an effective online class.

![](learning-3245793_1280.jpg)